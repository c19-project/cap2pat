<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Fast & Luxurious</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css2?family=Faster+One&display=swap" rel="stylesheet">

         {{-- Bootstrap CDN --}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <!-- aos -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        {{-- swiper css --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}">

        {{-- External CSS --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

        {{-- Fontawesome --}}
        <script src="https://kit.fontawesome.com/4de2b5cbee.js" crossorigin="anonymous"></script>


    <style>
        #homenav{
            background: linear-gradient(to right, black, indianred);
        }
        
    </style>
</head>
<body>
        
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark shadow-sm" id="homenav">
            <div class="container">
                <a class="navbar-brand headerstyle" href="{{ url('/') }}">
                    <img src="{{ asset('images/icons8-f-48.png') }}">
                    <span style="color: indianred;">F</span>ast & Luxurious
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="/">{{ __('Home') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#myfooter">{{ __('About') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#myfooter">{{ __('Contact Us') }}</a>
                            </li>

                            <li class="nav-item bg-dark rounded-pill border border-white">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                         
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="/home">{{ __('Home') }}</a>
                            </li>

                            @if(Auth::user()->role == 0)
                                <li class="nav-item">
                                    <a class="nav-link" href="/catalog">{{ __('Car List') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/cart">{{ __('Transaction') }}</a>
                                </li>
                                

                                <li class="nav-item">
                                    <a class="nav-link" href="/partner/add">{{ __('Be Our Partner') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#myfooter">{{ __('Contact Us') }}</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="/transactionhistory">{{ __('Transaction History') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/product/form">{{ __('Add Car') }}</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/productlist">{{ __('Car List') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/businesspartners">{{ __('Business Partners') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/partnerproduct/list">{{ __('Partnership Requests') }}</a>
                                </li>
                            @endif

                            <li class="nav-item dropdown bg-dark rounded-pill border border-white">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->role == 0)
                                    <a class="dropdown-item" href="/userorderhistory">{{ __('Transaction History') }}</a>
                                    @endif

                                  {{--   @if(Auth::user()->role == 1)
                                    <a class="dropdown-item" href="/productlist">{{ __('Car List') }}</a>
                                    @endif --}}

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

    {{-- SHOWCASE --}}
<div class="container-fluid">
    <div class="row" id="homepagebanner">

        <div class="col-lg-4">      
            <p class="text-center homebannerquote">"money may not buy happiness, but it's better to cry in a <b>LAMBORGHINI"</b></p>
            <hr>
            <p class="text-center">follow us:</p>
            <p class="text-center">
                <a class="homebannersoc mx-1" href=""><i class="fab fa-facebook-square"></i></a>
                <a class="homebannersoc mx-1" href=""><i class="fab fa-instagram"></i></a>
                <a class="homebannersoc mx-1" href=""><i class="fab fa-twitter"></i></a>
                <a class="homebannersoc mx-1" href=""><i class="fab fa-linkedin"></i></a>
            </p>
        </div>

        <div class="col-lg-5 offset-lg-2">
            
            <h1 class="homebannerheading text-center">Be <span style="font-family: 'Faster One', cursive; font-size: 140%;">F</span>ast & <span style="font-family: 'Faster One', cursive; font-size: 140%;">L</span>uxurious!</h1>
            <hr style="background-color: white;">
            <p  class=" homebannerpara text-center">"feel free to choose which one you would like to rent"</p>
            <p class="text-center">               
                <a class="mx-2 btn btn-lg btn-dark homebanner-btn1" href="{{ route('login') }}">Rent Now</a>
                         
                <a class="btn btn-lg btn-dark homebanner-btn2" href="#myfooter">Contact Us</a>
            </p>
        </div>

    </div>
</div>

{{-- PARTNER --}}
<div class="container-fluid" data-aos="fade-right">
    <div class="row" id="homepagebanner2">  
        <div class="col-lg-4 offset-lg-1">
            <h2 class="text-center homebannerheading2"><span class="headerstyle">D</span>o you need Extra Cash?</h2>
            <hr>
            <p class="text-center homebannerpara2">"if you have a car that you want to be rented, Let's work together!"</p>
            <p class="text-center">
                <a class="btn btn-lg btn-dark homebanner2-btn shadow" href="{{ route('login') }}">Proceed</a>
            </p>
        </div>          
    </div>
</div>

{{-- PARALAX --}}
<div class="container-fluid">
    <div class="row" id="homepageparalax">
        <div class="col-lg-7 mx-auto">
            <h2 class="text-center homeparalaxheading">Shift your day into a <span style="font-family: 'Faster One', cursive; font-size: 140%;">L</span>UXURIOUS One!</h2>
            <hr style="background-color: white;">
            <p class="text-center homeparalaxpara">"enjoy our LOWEST deals in the Country"</p>
            <p class="text-center">
                <a class="btn btn-lg btn-dark homeparalax-btn" href="{{ route('login') }}">Rent now</a>
            </p>
        </div>
    </div>
</div>

{{-- SWIPER CLIENTS --}}
<div class="container my-slider">
    <h2 class="text-center"><span class="headerstyle">M</span>eet Our Clients!</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- Swiper -->
            <div class="swiper-container" id="myswiper-container">
                <div class="swiper-wrapper">
      
                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                            <img src="{{ asset('images/2.jpg') }}">
                        </div>
                        <div class="details">
                            <p>"The cars are so good! Helped me to have an idea what car to buy"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                            <img src="{{ asset('images/3.jpg') }}">
                        </div>
                        <div class="details">
                             <p>"I love the Ferrari California. I look so cool while I'm driving that car"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                            <img src="{{ asset('images/4.jpg') }}">
                        </div>
                        <div class="details">
                             <p>"The cars are legit! Perfect for people who have some bucketlist out there!"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                             <img src="{{ asset('images/5.jpg') }}">
                        </div>
                        <div class="details">
                             <p>"Bumblebee is my favorite in the trasformers movie. The Camaro is so cool!"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                              <img src="{{ asset('images/6.jpg') }}">
                        </div>
                        <div class="details">
                              <p>"Lambos are the best! I recommend you guys to try the Reventon!"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                              <img src="{{ asset('images/7.jpg') }}">
                        </div>
                        <div class="details">
                             <p>"The cars run smoothly! Specially the Ferrari F12 which is my favorite!"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                             <img src="{{ asset('images/8.jpg') }}">
                        </div>
                        <div class="details">
                             <p>"The cars are great! I hope that there will be a Ford Mustang soon!"</p>
                        </div>
                    </div>

                    <div class="swiper-slide" id="myswiper">
                        <div class="imgBx">
                             <img src="{{ asset('images/9.jpg') }}">
                        </div>
                        <div class="details">
                              <p>"I'm so happy that I've tested driving my dream car!"</p>
                        </div>
                    </div>
      
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</div>

{{-- FOOTER --}}
<div class="container-fluid">
    <div class="row" id="myfooter">
        
        <div class="col-lg-4 offset-lg-2">
            <h2 class="disclaimer"><span class="headerstyle">D</span>isclaimer</h2>
            <hr style="background-color: indianred;">
            <p>All the images used in this website belong to the original owners</p>
            <p>This website is for educational purposes only</p>
            <p><strong>&copy; 2020 Fast & Luxurious Car Rentals</strong></p>
            <p><strong>Jan Patrick Reyes</strong></p>
        </div>

        <div class="col-lg-4">
            <h2 class="contactus"><span class="headerstyle">C</span>ontact or <span class="headerstyle">F</span>ollow us</h2>
            <hr style="background-color: indianred;">
            <p>#09123456789</p>
            <p>
                <a class="homebannersocf mx-1" href=""><i class="fab fa-facebook-square"></i></a>
                <a class="homebannersocf mx-1" href=""><i class="fab fa-instagram"></i></a>
                <a class="homebannersocf mx-1" href=""><i class="fab fa-twitter"></i></a>
                <a class="homebannersocf mx-1" href=""><i class="fab fa-linkedin"></i></a>
            </p>
        </div>

    </div>
</div>

    {{-- swiper js --}}
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>

    <!-- jquery lib -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    {{-- external js --}}
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

    <!-- aos -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</body>
</html>
<script>
var swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 800,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });

AOS.init({
        duration : 1200,
      });
</script>