@extends('template')
@section('title', 'Admin Page | Transaction History')
@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	
	
	<div class="container-fluid">
		<h2 class="text-center my-4"><span class="headerstyle">T</span>ransaction History</h2>
		<form class="form-inline mb-4">
      		<i class="fas fa-search search-icon mx-1"></i>
      		<input id="myInput" type="text" placeholder="Search.." class=" form-control border border-dark">
    	</form>
		<div class="row">
			<div class="col-md-11 mx-auto table-responsive text-center">
				<table class="table table-hover" style="-webkit-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  -moz-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61); border: 2px solid black;">
					<thead style="background-color: indianred; color: white;">
						<th scope="col">Client Name</th>
						<th scope="col">
							<form action="/transactionhistory" method="GET">
							@csrf
							<select name="by_purchase_date">
								<option value="asc">Ascending</option>
								<option value="desc">Descending</option>
							</select>
								<button class="btn btn-dark border border-white">By Date</button>
							</form>
						</th>
						<th scope="col">Total Rent Price</th>
						<th scope="col">Rented Car</th>
						<th scope="col">Day/s</th>
						<th scope="col">Action</th>
						<th scope="col"></th>
					</thead>

					<tbody id="myTable">
						@foreach($orders as $order)
						<tr>
							<td>{{ $order->user->name }}</td>
							<td>{{ $order->purchase_date->isoFormat('MMMM Do YYYY, h:mm:ss a') }}</td>
							<td>{{ $order->total_price }}</td>
							@foreach($order->products as $product)
								<td>{{ $product->product_name }}</td>
							@endforeach
							@foreach($order->products as $product)
								<td>{{ $product->pivot->quantity }}</td>
							@endforeach
							<td>
								<form action="/deleteorderhistory/{{ $order->id }}" method="POST">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger mt-1">Delete</button>
								</form>	
							</td>
							<td>
								<a href="/updateorderhistoryform/{{ $order->id }}" class="btn btn-success mt-1">Update</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
		<div class="col">
			<div class="jumbotron mt-4">
						<h1 class="display-4 text-center">Your Total Sales is: <strong>Php {{ $total_sales }}</strong></h1>
		</div>
	</div>
	</div>





@endsection