@extends('template')
@section('title', 'Admin Page | Update Transaction History')
@section('body')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 mx-auto mt-5">
				<form action="/updatetransactionhistory/{{ $orderhistory->id }}" method="POST">
					@csrf
					@method('PATCH')
					<label>Days:</label>
					@foreach($orderhistory->products as $product)
					<input type="number" name="days" class="form-control" value="{{ $product->pivot->quantity }}">
					@endforeach
					<button type="submit" class="btn btn-success mt-3">Update</button>
					<a href="/transactionhistory" class="btn btn-danger mt-3">Cancel</a>
				</form>				
			</div>
		</div>	
	</div>
@endsection