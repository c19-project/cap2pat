@extends('template')
@section('title', 'Fast & Luxurious | Transaction History')
@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	
	
	<div class="container-fluid">
		<h2 class="text-center mb-4"><span class="headerstyle">T</span>ransaction History</h2>
		<form class="form-inline mb-4">
      		<i class="fas fa-search search-icon mx-1"></i>
      		<input id="myInput" type="text" placeholder="Search.." class=" form-control border border-dark">
    	</form>
		<div class="row">
			<div class="col-md-11 mx-auto table-responsive">
				<table class="table table-hover" style="-webkit-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  -moz-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61); border: 2px solid black;">
					<thead style="background-color: indianred; color: white;">
						
						<th scope="col">
							<form action="/userorderhistory" method="GET">
							@csrf
							<select name="by_purchase_date">
								<option value="asc">Ascending</option>
								<option value="desc">Descending</option>
							</select>
								<button class="btn btn-dark border border-white">By Date</button>
							</form>
						</th>
						<th scope="col">Total Rent Price</th>
						<th scope="col">Rented Car</th>
						<th scope="col">Day/s</th>
					</thead>

					<tbody id="myTable">
						@foreach($orders as $order)

						@if(Auth::user()->id == $order->user_id)
						<tr>
							
							<td>{{ $order->purchase_date->isoFormat('MMMM Do YYYY, h:mm:ss a') }}</td>
							<td>{{ $order->total_price }}</td>
							@foreach($order->products as $product)
								<td>{{ $product->product_name }}</td>
							@endforeach
							@foreach($order->products as $product)
								<td>{{ $product->pivot->quantity }}</td>
							@endforeach

						</tr>
						@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	{{-- FOOTER --}}
	<div class="container-fluid">
		<div class="row" id="myfooter">
			
			<div class="col-lg-4 offset-lg-2">
				<h2 class="disclaimer"><span class="headerstyle">D</span>isclaimer</h2>
				<hr style="background-color: indianred;">
				<p>All the images used in this website belong to the original owners</p>
				<p>This website is for educational purposes only</p>
				<p><strong>&copy; 2020 Fast & Luxurious Car Rentals</strong></p>
				<p><strong>Jan Patrick Reyes</strong></p>
			</div>

			<div class="col-lg-4">
				<h2 class="contactus"><span class="headerstyle">C</span>ontact or <span class="headerstyle">F</span>ollow us</h2>
				<hr style="background-color: indianred;">
				<p>#09123456789</p>
				<p>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-facebook-square"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-instagram"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-twitter"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-linkedin"></i></a>
				</p>
			</div>

		</div>
	</div>



@endsection