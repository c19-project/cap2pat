@extends('template')
@section('title', 'Fast & Luxurious | My Transaction')
@section('body')

	
	
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-md-10 mx-auto table-responsive">
			@if(session('message'))
				<div class="alert alert-primary" role="alert">
					{{ session('message') }}
				</div>
			@endif
				{{-- {{ $total }}
				@dd($product_cart) --}}
			@if($product_cart != null)

				<table class="table">
					<thead style="background-color: indianred; color: white;">
						<tr>
							<th scope="col">Product</th>
							<th scope="col">No. of days</th>
							<th scope="col">Price</th>
							<th scope="col">Subtotal</th>
							<th scope="col">Action</th>
						</tr>
					</thead>

					<tbody>
						@foreach($product_cart as $product)
							<tr>
								<td>{{ $product->product_name }}</td>
								<td>
									<form action="/cart/{{ $product->id }}" method="POST">
										@csrf
										@method('PUT')
										<input type="number" name="newQuantity" value="{{ $product->quantity }}">
										<button type="submit" class="btn btn-success">Update</button>
									</form>
									

								</td>
								<td>{{ $product->price }}</td>
								<td>{{ $product->subtotal }}</td>
								<td>
									<form action="/cart/{{ $product->id }}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-danger" type="submit">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach

						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>{{ $total }}</td>
							<td>
								<form action="/orders" method="POST">
									@csrf
									<select name="payment_mode" id="">
										@foreach($payment_modes as $payment_mode)
										<option value="{{ $payment_mode->id }}">{{ $payment_mode->payment_mode_name }}</option>
										@endforeach
									</select>
									<button type="submit" class="btn btn-success">Check out</button>
								</form>
							</td>							
						</tr>

						<tr>
							<td>
								<a href="/clearcart" class="btn btn-danger">Cancel Transaction</a>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				
				@else

					<div class="container-fluid" data-aos="fade-right">
						<div class="row" id="notrans">	
							<div class="col-lg-4 offset-lg-1">
								<h2 class="text-center notransheading"><span class="headerstyle">Y</span>ou haven't rented any cars yet!</h2>
								<hr>
								<p class="text-center homebannerpara2">"Check out for some cars!"</p>
								<p class="text-center">
									@if(Auth::user()->role == 0)
										<a class="btn btn-lg btn-dark homebanner2-btn shadow" href="/catalog">Proceed</a>
									@else
										<a class="btn btn-lg btn-dark homebanner2-btn shadow" href="/home">Proceed</a>
									@endif
								</p>
							</div>			
						</div>
					</div>

				@endif
			</div>
		</div>
	</div>

	{{-- FOOTER --}}
	<div class="container-fluid">
		<div class="row" id="myfooter">
			
			<div class="col-lg-4 offset-lg-2">
				<h2 class="disclaimer"><span class="headerstyle">D</span>isclaimer</h2>
				<hr style="background-color: indianred;">
				<p>All the images used in this website belong to the original owners</p>
				<p>This website is for educational purposes only</p>
				<p><strong>&copy; 2020 Fast & Luxurious Car Rentals</strong></p>
				<p><strong>Jan Patrick Reyes</strong></p>
			</div>

			<div class="col-lg-4">
				<h2 class="contactus"><span class="headerstyle">C</span>ontact or <span class="headerstyle">F</span>ollow us</h2>
				<hr style="background-color: indianred;">
				<p>#09123456789</p>
				<p>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-facebook-square"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-instagram"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-twitter"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-linkedin"></i></a>
				</p>
			</div>

		</div>
	</div>

@endsection