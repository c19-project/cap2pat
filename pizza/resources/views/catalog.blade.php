@extends('template')

@section('title', 'Fast & Luxurious | Catalog Page')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable .card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	

	<div class="container mt-4" id="carlist">
		
		@if(session('message'))
			<div class="alert alert-success alert-dismissible mt-4" role="alert">
				{{ session('message') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif

		<form class="form-inline">
      		<i class="fas fa-search search-icon mx-1"></i>
      		<input id="myInput" type="text" placeholder="Search.." class=" form-control border border-dark">
    	</form>

		<div class="row">

				{{-- card container for products --}}
				@foreach($products as $product)
					
					<div class="col-md-4 mt-4" id="myTable">
							
						<div class="card h-100" id="mycard" style="width: 18rem; border-bottom-right-radius: 20%; border: 2px solid black; -webkit-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  -moz-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);">
						  <img class="card-img-top img-fluid" src="{{ asset($product->image) }}" alt="Card image cap" >
						  	<div class="card-body" style="background: linear-gradient(to right, black, indianred);
                border-bottom-right-radius: 20%;">
							    <h5 class="card-title text-center" style="font-family: 'Sans-serif'; color: white; font-weight: bold;">"{{ $product->product_name }}"</h5>
							    <p class="card-text" style="color: white;">Rent price: <span style="color: white; text-decoration: underline;">Php {{ $product->price }}/day</span></p>
							    <p class="card-text" style="color: white;">"{{ $product->description }}"</p> 
							    <hr style="background-color: white;">
							    {{-- add to cart form --}}
							    {{-- Goal: get the product ID and the quantity --}}
							    @if($product->estado != "inuse")
							    <form action="/cart" method="POST" class="form-inline">
							    	@csrf
							    	<input type="number" name="product_id" value="{{ $product->id }}" hidden>
							    	<input type="number" name="quantity" class="form-control text-center mx-auto border border-dark" placeholder="No. of days">

							    	<button type="submit" class="btn btn-dark btn-block mt-1 rounded-pill border border-light">Rent</button>
							    </form>
							    @else
							    	<h4 class="card-text text-center" style="color: white; text-shadow: 4px 5px 5px rgba(33,14,9,0.75);">"Currently In-Use"</h4>
							    @endif

						  	</div>
						</div>

					</div>
				@endforeach
		</div>
	</div>

	{{-- PARTNER --}}
	<div class="container-fluid" data-aos="fade-right">
		<div class="row" id="homepagebanner2">	
			<div class="col-lg-4 offset-lg-1">
				<h2 class="text-center homebannerheading2"><span class="headerstyle">D</span>o you need Extra Cash?</h2>
				<hr>
				<p class="text-center homebannerpara2">"if you have a car that you want to be rented, Let's work together!"</p>
				<p class="text-center">
					@if(Auth::user()->role == 0)
						<a class="btn btn-lg btn-dark homebanner2-btn shadow" href="/partner/add">Proceed</a>
					@else
						<a class="btn btn-lg btn-dark homebanner2-btn shadow" href="/home">Proceed</a>
					@endif
				</p>
			</div>			
		</div>
	</div>

	{{-- FOOTER --}}
	<div class="container-fluid">
		<div class="row" id="myfooter">
			
			<div class="col-lg-4 offset-lg-2">
				<h2 class="disclaimer"><span class="headerstyle">D</span>isclaimer</h2>
				<hr style="background-color: indianred;">
				<p>All the images used in this website belong to the original owners</p>
				<p>This website is for educational purposes only</p>
				<p><strong>&copy; 2020 Fast & Luxurious Car Rentals</strong></p>
				<p><strong>Jan Patrick Reyes</strong></p>
			</div>

			<div class="col-lg-4">
				<h2 class="contactus"><span class="headerstyle">C</span>ontact or <span class="headerstyle">F</span>ollow us</h2>
				<hr style="background-color: indianred;">
				<p>#09123456789</p>
				<p>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-facebook-square"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-instagram"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-twitter"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-linkedin"></i></a>
				</p>
			</div>

		</div>
	</div>
	








@endsection