@extends('layouts.app')

@section('content')

{{-- SHOWCASE --}}
<div class="container-fluid">
	<div class="row" id="homepagebanner">

		<div class="col-lg-4">		
			<p class="text-center homebannerquote">"money may not buy happiness, but it's better to cry in a <b>LAMBORGHINI"</b></p>
			<hr>
			<p class="text-center">follow us:</p>
			<p class="text-center">
				<a class="homebannersoc	mx-1" href=""><i class="fab fa-facebook-square"></i></a>
				<a class="homebannersoc mx-1" href=""><i class="fab fa-instagram"></i></a>
				<a class="homebannersoc mx-1" href=""><i class="fab fa-twitter"></i></a>
				<a class="homebannersoc mx-1" href=""><i class="fab fa-linkedin"></i></a>
			</p>
		</div>

		<div class="col-lg-5 offset-lg-2">
			
			<h1 class="homebannerheading text-center">Be <span style="font-family: 'Faster One', cursive; font-size: 140%;">F</span>ast & <span style="font-family: 'Faster One', cursive; font-size: 140%;">L</span>uxurious!</h1>
			<hr style="background-color: white;">
			<p  class=" homebannerpara text-center">"feel free to choose which one you would like to rent"</p>
			<p class="text-center">
				@if(Auth::user()->role == 0)
					<a class="mx-2 btn btn-lg btn-dark homebanner-btn1" href="/catalog">Rent Now</a>
				@else
					<a class="mx-2 btn btn-lg btn-dark homebanner-btn1" href="/home">Rent Now</a>
				@endif
				<a class="btn btn-lg btn-dark homebanner-btn2" href="#myfooter">Contact Us</a>
			</p>
		</div>

	</div>
</div>

{{-- PARTNER --}}
<div class="container-fluid" data-aos="fade-right">
	<div class="row" id="homepagebanner2">	
		<div class="col-lg-4 offset-lg-1">
			<h2 class="text-center homebannerheading2"><span class="headerstyle">D</span>o you need Extra Cash?</h2>
			<hr>
			<p class="text-center homebannerpara2">"if you have a car that you want to be rented, Let's work together!"</p>
			<p class="text-center">
				@if(Auth::user()->role == 0)
					<a class="btn btn-lg btn-dark homebanner2-btn shadow" href="/partner/add">Proceed</a>
				@else
					<a class="btn btn-lg btn-dark homebanner2-btn shadow" href="/home">Proceed</a>
				@endif
			</p>
		</div>			
	</div>
</div>

{{-- PARALAX --}}
<div class="container-fluid">
	<div class="row" id="homepageparalax">
		<div class="col-lg-7 mx-auto">
			<h2 class="text-center homeparalaxheading">Shift your day into a <span style="font-family: 'Faster One', cursive; font-size: 140%;">L</span>UXURIOUS One!</h2>
			<hr style="background-color: white;">
			<p class="text-center homeparalaxpara">"enjoy our LOWEST deals in the Country"</p>
			<p class="text-center">
				@if(Auth::user()->role == 0)
					<a class="btn btn-lg btn-dark homeparalax-btn" href="/catalog">Rent now</a>
				@else
					<a class="btn btn-lg btn-dark homeparalax-btn" href="/home">Rent now</a>
				@endif
			</p>
		</div>
	</div>
</div>

{{-- SWIPER CLIENTS --}}
<div class="container my-slider">
	<h2 class="text-center"><span class="headerstyle">M</span>eet Our Clients!</h2>
	<div class="row">
  		<div class="col-md-12">
  			<!-- Swiper -->
  			<div class="swiper-container" id="myswiper-container">
    			<div class="swiper-wrapper">
      
				    <div class="swiper-slide" id="myswiper">
				     	<div class="imgBx">
				         	<img src="{{ asset('images/2.jpg') }}">
				        </div>
				        <div class="details">
				         	<p>"The cars are so good! Helped me to have an idea what car to buy"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				          	<img src="{{ asset('images/3.jpg') }}">
				        </div>
				        <div class="details">
				         	 <p>"I love the Ferrari California. I look so cool while I'm driving that car"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				          	<img src="{{ asset('images/4.jpg') }}">
				        </div>
				        <div class="details">
				         	 <p>"The cars are legit! Perfect for people who have some bucketlist out there!"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				         	 <img src="{{ asset('images/5.jpg') }}">
				        </div>
				        <div class="details">
				         	 <p>"Bumblebee is my favorite in the trasformers movie. The Camaro is so cool!"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				        	  <img src="{{ asset('images/6.jpg') }}">
				        </div>
				        <div class="details">
				        	  <p>"Lambos are the best! I recommend you guys to try the Reventon!"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				        	  <img src="{{ asset('images/7.jpg') }}">
				        </div>
				        <div class="details">
				         	 <p>"The cars run smoothly! Specially the Ferrari F12 which is my favorite!"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				         	 <img src="{{ asset('images/8.jpg') }}">
				        </div>
				        <div class="details">
				         	 <p>"The cars are great! I hope that there will be a Ford Mustang soon!"</p>
				        </div>
				    </div>

				    <div class="swiper-slide" id="myswiper">
				        <div class="imgBx">
				         	 <img src="{{ asset('images/9.jpg') }}">
				        </div>
				        <div class="details">
				        	  <p>"I'm so happy that I've tested driving my dream car!"</p>
				        </div>
				    </div>
      
    			</div>
    			<!-- Add Pagination -->
    			<div class="swiper-pagination"></div>
  			</div>
		</div>
	</div>
</div>

{{-- FOOTER --}}
<div class="container-fluid">
	<div class="row" id="myfooter">
		
		<div class="col-lg-4 offset-lg-2">
			<h2 class="disclaimer"><span class="headerstyle">D</span>isclaimer</h2>
			<hr style="background-color: indianred;">
			<p>All the images used in this website belong to the original owners</p>
			<p>This website is for educational purposes only</p>
			<p><strong>&copy; 2020 Fast & Luxurious Car Rentals</strong></p>
			<p><strong>Jan Patrick Reyes</strong></p>
		</div>

		<div class="col-lg-4">
			<h2 class="contactus"><span class="headerstyle">C</span>ontact or <span class="headerstyle">F</span>ollow us</h2>
			<hr style="background-color: indianred;">
			<p>#09123456789</p>
			<p>
				<a class="homebannersocf mx-1" href=""><i class="fab fa-facebook-square"></i></a>
				<a class="homebannersocf mx-1" href=""><i class="fab fa-instagram"></i></a>
				<a class="homebannersocf mx-1" href=""><i class="fab fa-twitter"></i></a>
				<a class="homebannersocf mx-1" href=""><i class="fab fa-linkedin"></i></a>
			</p>
		</div>

	</div>
</div>

@endsection
