<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title')</title>

  <!-- fav icon -->
    <link rel="icon" href="{{ asset('images/icons8-f-48.png') }}">

	{{-- Bootstrap CDN --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    {{-- Fontawesome --}}
    <script src="https://kit.fontawesome.com/4de2b5cbee.js" crossorigin="anonymous"></script>

    {{-- External CSS --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

    {{-- swiper css --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}">

    {{-- google fonts --}}
    <link href="https://fonts.googleapis.com/css2?family=Faster+One&display=swap" rel="stylesheet">

    <!-- aos -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

</head>
<body>
  <nav class="navbar sticky-top navbar-expand-lg navbar-dark shadow-sm" id="homenav">
            <div class="container">
                <a class="navbar-brand headerstyle" href="{{ url('/') }}">
                    <img src="{{ asset('images/icons8-f-48.png') }}">
                    <span style="color: indianred;">F</span>ast & Luxurious
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="/">{{ __('Home') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#">{{ __('About') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#myfooter">{{ __('Contact Us') }}</a>
                            </li>

                            <li class="nav-item bg-dark rounded-pill border border-white">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                         
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="/home">{{ __('Home') }}</a>
                            </li>

                            @if(Auth::user()->role == 0)
                                <li class="nav-item">
                                    <a class="nav-link" href="/catalog">{{ __('Car List') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/cart">{{ __('Transaction') }}</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/partner/add">{{ __('Be Our Partner') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#myfooter">{{ __('Contact Us') }}</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="/transactionhistory">{{ __('Transaction History') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/product/form">{{ __('Add Car') }}</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/productlist">{{ __('Car List') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/businesspartners">{{ __('Business Partners') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="/partnerproduct/list">{{ __('Partnership Requests') }}</a>
                                </li>
                            @endif

                            <li class="nav-item dropdown bg-dark rounded-pill border border-white">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->role == 0)
                                    <a class="dropdown-item" href="/userorderhistory">{{ __('Transaction History') }}</a>
                                    @endif

                                {{--     @if(Auth::user()->role == 1)
                                    <a class="dropdown-item" href="/productlist">{{ __('Admin') }}</a>
                                    @endif --}}

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

	@yield('body')


    {{-- swiper js --}}
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>

	<!-- jquery lib -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    {{-- external js --}}
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

    <!-- aos -->
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

   <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</body>
</html>
<script>
var swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 800,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });

AOS.init({
        duration : 1200,
      });
</script>
