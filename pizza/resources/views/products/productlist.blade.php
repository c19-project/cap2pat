@extends('template')
@section('title', 'Admin Page | Product List')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
	
	

	<div class="container-fluid">
		<h2 class="text-center my-4"><span class="headerstyle">C</span>ar List</h2>
		<form class="form-inline mb-4">
      		<i class="fas fa-search search-icon mx-1"></i>
      		<input id="myInput" type="text" placeholder="Search.." class=" form-control border border-dark">
    	</form>
		<div class="row">
			<div class="col-md-12 mx-auto table-responsive text-center">
				
				<table class="table table-hover" style="-webkit-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  -moz-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61); border: 2px solid black;">
					
					<thead style="background-color: indianred; color: white;">
						<tr>
							<th scope="col">
								<form action="/productlist" method="GET">
									@csrf
									<select name="by_product_name">
										<option value="asc">Asending</option>
										<option value="desc">Descending</option>
									</select>
									<button type="submit" class="btn btn-dark border border-white">By name</button>		
								</form>
							</th>
							<th scope="col">Car Rent</th>
							<th scope="col">Car description</th>
							<th scope="col">Status</th>
							<th scope="col">Car Image</th>
							<th scope="col">Action</th>
							<th scope="col"></th>
						</tr>
					</thead>

					<tbody id="myTable">
						@foreach($products as $product)
						<tr>
							<td>{{ $product->product_name }}</td>
							<td>{{ $product->price }}</td>
							<td>{{ $product->description }}</td>
							<td>{{ $product->estado }}</td>
							<td><img src="{{ $product->image }}"></td>
							<td>		
								{{-- <a href="/product/delete/{{ $product->id }}" class="btn btn-danger mt-1">Delete</a> --}}
								<button class="deleteProductButton btn btn-danger mt-1" data-id="{{ $product->id }}">Delete</button>			
							</td>
							<td>
								<a href="/product/form/update/{{ $product->id }}" class="btn btn-success mt-1">Update</a> 
							</td>
						</tr>
						@endforeach()
					</tbody>


				</table>


			</div>
		</div>
	</div>

<script type="text/javascript">
		
		let deleteButtons = document.querySelectorAll('.deleteProductButton');

		const CSRFTOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

		deleteButtons.forEach(function(deleteButton) {
			
			let productID = deleteButton.getAttribute("data-id");
			
			let productTobeRemoved = deleteButton.parentElement.parentElement;


			deleteButton.addEventListener("click", function(){

				fetch('/product/delete/'+productID, {
					method: "get",
					// body: 
					headers: {'X-CSRF-TOKEN' : CSRFTOKEN }
				})
				
				.then(function(responseFromOurServer){
					
					return responseFromOurServer.text();
				})
				.then(function(theResponseThatWillBeReturnToOurPage){
					productTobeRemoved.remove();
				})
			});

		});

	</script>



@endsection