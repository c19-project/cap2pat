@extends('template')
@section('title', 'Admin Page | Add Product')

@section('body')

	
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 mx-auto mt-3 addcarcol">

				{{-- Validation Error response --}}
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<form action="/product/form/add" method="POST" enctype="multipart/form-data">
					@csrf

					<label style="color: indianred; font-weight: bold;">Car name:</label>
					<input type="text" name="product_name" class="form-control">

					<label style="color: indianred; font-weight: bold;">Price:</label>
					<input type="number" name="product_price" class="form-control">

					<label style="color: indianred; font-weight: bold;">Description:</label>
					<textarea name="product_description" class="form-control"></textarea>

					<label style="color: indianred; font-weight: bold;">Category:</label>
					<select name="product_category" class="form-control">
						<option value="">Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->category_name }}</option>
						@endforeach
					</select>

					<label style="color: indianred; font-weight: bold;">Status:</label>
					<input type="text" name="product_status" class="form-control">

					<label style="color: indianred; font-weight: bold;">Upload Image:</label>
					<input type="file" name="product_image" class="form-control">

					<button type="submit" class="btn btn-dark btn-block mt-2 loginbtn">Submit</button>

				</form>

			</div>
		</div>
	</div>






@endsection
	


