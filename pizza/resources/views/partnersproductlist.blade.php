@extends('template')
@section('title', 'Admin Page | Partners Cars')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	

	<div class="container-fluid" style="margin-bottom: 3%;">
		<h2 class="text-center my-4"><span class="headerstyle">P</span>artnership Requests</h2>
		<form class="form-inline mb-4">
      		<i class="fas fa-search search-icon mx-1"></i>
      		<input id="myInput" type="text" placeholder="Search.." class=" form-control border border-dark">
    	</form>
		<div class="row">
			<div class="col-md-12 mx-auto table-responsive text-center">
				<table class="table table-hover" style="-webkit-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  -moz-box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61);
  box-shadow: 6px 21px 22px 0px rgba(0,0,0,0.61); border: 2px solid black;">
					
					<thead style="background-color: indianred; color: white;">
						<th scope="col">Partner Name</th>
						<th scope="col">Email</th>
						<th scope="col">Car Name</th>
						<th scope="col">Price</th>
						<th scope="col">Description</th>
						<th scope="col">Image</th>
						<th scope="col">Action</th>
						<th scope="col"></th>
					</thead>

					<tbody id="myTable">
						@foreach($partners as $partner)
						<tr>
							<td>{{ $partner->name }}</td>
							<td>{{ $partner->user->email }}</td>
							<td>{{ $partner->product_name }}</td>
							<td>{{ $partner->price }}</td>
							<td>{{ $partner->description }}</td>
							<td><img src="{{ asset($partner->image) }}"></td>
							<td>
								<a href="/partnerapprove/{{ $partner->id }}}" class="btn btn-success mt-1">Approve</a>
							</td>
							<td>
								{{-- <a href="/partnerdeleteproduct/{{ $partner->id }}" class="btn btn-danger">Decline</a> --}}
								<button class="deleteProductButton btn btn-danger mt-1" data-id="{{ $partner->id }}">Decline</button>
							</td>
						</tr>
						@endforeach
					</tbody>



				</table>
			</div>
		</div>
	</div>

<script type="text/javascript">
		
		let deleteButtons = document.querySelectorAll('.deleteProductButton');

		const CSRFTOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

		deleteButtons.forEach(function(deleteButton) {
			
			let productID = deleteButton.getAttribute("data-id");
			
			let productTobeRemoved = deleteButton.parentElement.parentElement;


			deleteButton.addEventListener("click", function(){

				fetch('/partnerdeleteproduct/'+productID, {
					method: "get",
					// body: 
					headers: {'X-CSRF-TOKEN' : CSRFTOKEN }
				})
				
				.then(function(responseFromOurServer){
					
					return responseFromOurServer.text();
				})
				.then(function(theResponseThatWillBeReturnToOurPage){
					productTobeRemoved.remove();
				})
			});

		});

	</script>

@endsection