@extends('template')
@section('title', 'Partner Page | Add car')
@section('body')

	
	<div class="container partneradd">
		@if(session('message'))
			<div class="alert alert-success alert-dismissible mt-4" role="alert">
				{{ session('message') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif
		
		<div class="row">
			<div class="col-md-8 mx-auto mt-3 partneraddcol">

				{{-- Validation Error response --}}
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<form action="/partnerproduct/form/add" method="POST" enctype="multipart/form-data">
					@csrf

					<label style="color: indianred; font-weight: bold;">Car name:</label>
					<input type="text" name="partner_product_name" class="form-control">

					<label style="color: indianred; font-weight: bold;">Price:</label>
					<input type="number" name="partner_product_price" class="form-control">

					<label style="color: indianred; font-weight: bold;">Description:</label>
					<textarea name="partner_product_description" class="form-control"></textarea>

					<label style="color: indianred; font-weight: bold;">Category:</label>
					<select name="partner_product_category" class="form-control">
						<option value="">Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->category_name }}</option>
						@endforeach
					</select>

					<label style="color: indianred; font-weight: bold;">Upload Image:</label>
					<input type="file" name="partner_product_image" class="form-control">

					<button type="submit" class="btn btn-dark btn-block mt-2 loginbtn">Submit</button>

				</form>

			</div>
		</div>
	</div>

	{{-- FOOTER --}}
	<div class="container-fluid">
		<div class="row" id="myfooter">
			
			<div class="col-lg-4 offset-lg-2">
				<h2 class="disclaimer"><span class="headerstyle">D</span>isclaimer</h2>
				<hr style="background-color: indianred;">
				<p>All the images used in this website belong to the original owners</p>
				<p>This website is for educational purposes only</p>
				<p><strong>&copy; 2020 Fast & Luxurious Car Rentals</strong></p>
				<p><strong>Jan Patrick Reyes</strong></p>
			</div>

			<div class="col-lg-4">
				<h2 class="contactus"><span class="headerstyle">C</span>ontact or <span class="headerstyle">F</span>ollow us</h2>
				<hr style="background-color: indianred;">
				<p>#09123456789</p>
				<p>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-facebook-square"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-instagram"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-twitter"></i></a>
					<a class="homebannersocf mx-1" href=""><i class="fab fa-linkedin"></i></a>
				</p>
			</div>

		</div>
	</div>

@endsection