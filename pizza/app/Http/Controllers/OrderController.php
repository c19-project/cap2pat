<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Carbon\Carbon;
use App\Product;
use Auth;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newOrder = new Order();
            // set the values
            $newOrder->transaction_code = "FAL".time(); // get the exact time using time(). returns a unix time epoch
            $newOrder->user_id = Auth::user()->id;
            $newOrder->purchase_date = Carbon::now();
            $newOrder->payment_mode_id = $request->payment_mode;
            $newOrder->status_id = 1;
            $newOrder->total_price = 0; // initial total price of order, since we did not have the product of quantity * products' price
            // save the order to the orders table
            $newOrder->save();

            $total = 0;

            // to fill in the products in the orders (pivot table = order_product, let's iterate first our session cart)
            foreach ($request->session()->get('cart') as $id => $quantity) {
                // to get the product details, we'll find the Product using the cart's $id. Then assign it to $product
                $product = Product::find($id);
                // get the subtotal for the product
                $subtotal = $quantity * $product->price;
                $total += $subtotal;
                $product->estado = "inuse"; 
                // on creating records on our pivot table, we use the attach(). and for the delete, we use the detach() method
                $newOrder->products()->attach($id, ["quantity" => $quantity]); // quantity key name is based on the additional column on our pivot table
                $product->save();
            }
            $updateTotalPriceOfOrder = Order::find($newOrder->id);
            $updateTotalPriceOfOrder->total_price = $total;
            $updateTotalPriceOfOrder->save();

            Session::forget("cart");
            // $request->session()->forget("cart");
            Session::flash("message", "Thank you for renting! You may now use the car! Your reference no. is ".$newOrder->transaction_code.".");
            return redirect('/cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
