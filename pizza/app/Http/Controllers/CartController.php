<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;
use App\Payment_mode;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_cart = [];
            if (Session::has('cart')) {
                // get the session cart then assign it to $cart
                $cart = Session::get('cart');
                // $total is for the total price, we'll git it an initial value of 0
                $total = 0;

        
            foreach ($cart as $product_id => $quantity) {
                
                $product = Product::find($product_id);
               
                // declare a quantity property on our $product for later use
                $product->quantity = $quantity; // this product quantity is only available on our session cart
                // declare a subtotal property for later use
                $product->subtotal = $quantity * $product->price;

                $total += $product->subtotal;

                
                // now that we have the product details, let's "push" the $product to our $product_cart
                $product_cart[] = $product;
            }
            $payment_modes = Payment_mode::all();
            return view('cart', compact('product_cart', 'total', 'payment_modes'));
        }

        return view('cart', compact('product_cart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required|integer'
        ]);

        $id = $request->product_id;
        $quantity = $request->quantity;
        
       

        // 1st, we have to check if there is a session named cart
        if ($request->session()->has('cart')) {
             // if we do have the session named cart, get it and save it as a variable named $cart
            $cart = $request->session()->get('cart'); 
        } 
        else {
             // else lets initialize an empty array $cart
            $cart = [];
        }

        // search in the session cart if it has a key($id) matching submitted product $id
        if (isset($cart[$id])) {
            // if yes, we'll increment with the newly submitted product quantity in the $cart
            $cart[$id] += $quantity;
        }
        else {
            // else we'll set a new key($id) and new value($quantity) in the $cart
            $cart[$id] = $quantity; 
        }

        // put the $cart variable as a session cart value
        $request->session()->put('cart', $cart);

        // set a session message that will notify the user after adding products to the session cart
        // we'll be using the session flash helper
        $request->session()->flash('message', 'A rent request has been added to your transaction!');
        return redirect('/catalog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                "newQuantity" => 'required|integer'
            ]);

            // get the session cart and assign it to $cart
            $cart = $request->session()->get('cart');
            //dd($cart[$id]); // use this, to target the specific product on our session cart. This will return the value(quantity)

            // set the newly received quantity from our cart as the current quantity of this specific product ($id) in the $cart variable
            $cart[$id] = $request->newQuantity;
            // put the $cart variable as a session cart value
            $request->session()->put('cart', $cart);

            $request->session()->flash('message', 'The number of days has been changed to '.$request->newQuantity.'.');

            return redirect('/cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Session::forget("cart.$id"); /*Now this will forget the specific item (using its $id) on our session named cart*/
        // echo 'This will return a string even if the $variable is declared inside'; // returns $variable, instead the value of the variable
        // echo "$variable"; // returns the value of the variable declared inside

        Session::flash("message", "Car has been removed from the transaction!");
        return redirect('/cart');
    }

    public function clear(){
        /*
            empty the session cart
        */
            Session::forget("cart");
            return redirect('/cart');
    }
}
