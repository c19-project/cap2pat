<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partner;
use App\Product;
use App\Order;

class AdminController extends Controller
{
    function viewPartnerProducts(){
    	$partners = Partner::all();
    	return view('partnersproductlist', compact('partners'));
    }
    function deletePartnerProducts($id){
        $partner = Partner::find($id);
        $partner->delete();
        
        return redirect('/partnerproduct/list');
    }

    function approveProduct($id){
    	$partner = Partner::find($id);
    	$newProduct = new Product();
    	$newProduct->product_name = $partner->product_name;
    	$newProduct->price = $partner->price;
    	$newProduct->description = $partner->description;
    	$newProduct->category_id = $partner->category_id;
    	$newProduct->estado = "available";
    	$newProduct->image = $partner->image;
    	$newProduct->save();
        
    	return redirect('/productlist');
    }

    function viewOrderHistory(Request $request){
    	if (isset($request->by_purchase_date)) {
            $orders = Order::orderBy('purchase_date', $request->by_purchase_date)->get();
        }
        else {
           $orders = Order::all();
        }

        $total_sales = 0;
        $rows = Order::all();
        foreach ($rows as $row) {
            $total_sales += $row->total_price;
        }

    	return view('orderhistory', compact('orders', 'total_sales'));
    }

    function viewBusinessPartners(){
        $partners = Partner::all();
        return view('partnerslist', compact('partners'));
    }

    function viewUserTransHistory(){
        if (isset($request->by_purchase_date)) {
            $orders = Order::orderBy('purchase_date', $request->by_purchase_date)->get();
        }
        else {
           $orders = Order::all();
        }
        return view('userorderhistory', compact('orders'));
    }

    function deleteTransactionHistory($id){
        $deleteTransactionHistory = Order::find($id);
        $deleteTransactionHistory->delete();

        return redirect('/transactionhistory');
    }

    function viewUpdateTransactionHistory($id){
        $orderhistory = Order::find($id);
        return view('updateorderhistory', compact('orderhistory'));
    }

    function updateTransactionHistory(Request $request, $id){
        $quantity = $request->days;
        $updateOrderHistory = Order::find($id);
        foreach ($updateOrderHistory->products as $product) {
           $product_id =  $product->pivot->product_id;
        }
        $updateOrderHistory->products()->updateExistingPivot($product_id, ["quantity" => $quantity]);
        $product = Product::find($product_id);
        $updateOrderHistory->total_price = $product->price * $quantity;

        $updateOrderHistory->save();
        return redirect('/transactionhistory');
    }
}
