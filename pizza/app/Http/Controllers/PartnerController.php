<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Partner;
use Auth;

class PartnerController extends Controller
{
    function viewPartnerAddProductForm(){
    	$categories = Category::all();
    	return view('partneraddproductform', compact('categories'));
    }

    function partnerAddProduct(Request $request){

    	$request->validate([
    		'partner_product_name' => 'required|string', 
    		'partner_product_price' => 'required|numeric',
    		'partner_product_description' => 'required|string',
    		'partner_product_category' => 'required',
    		'partner_product_image' => 'required|image'
    	]);

    	$product_name = htmlspecialchars($request->partner_product_name);
    	$product_price = htmlspecialchars($request->partner_product_price);
    	$product_description = htmlspecialchars($request->partner_product_description);
    	$product_category = htmlspecialchars($request->partner_product_category);
    	
    	$product_image = $request->file('partner_product_image');

    	$newPartnerProduct = new Partner();
    	$newPartnerProduct->name = Auth::user()->name;
    	$newPartnerProduct->user_id = Auth::user()->id;
    	
    	$newPartnerProduct->product_name = $product_name;
    	$newPartnerProduct->price = $product_price;
    	$newPartnerProduct->description = $product_description;
    	$newPartnerProduct->category_id = $product_category;
    	$newPartnerProduct->estado = "available";

    	$file_name = time().".".$product_image->getClientOriginalExtension();

    	$file_destination = "partnerimages/";
  	
    	$product_image->move($file_destination, $file_name);
	
    	$newPartnerProduct->image = $file_destination.$file_name;

    	$newPartnerProduct->save();
        $request->session()->flash('message', 'A partnership request has been sent! Please wait for our approval');
    	return redirect('/partner/add');
    }
}
