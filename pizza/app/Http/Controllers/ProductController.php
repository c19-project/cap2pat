<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    function viewAddProductPage(){
    	
    	$categories = Category::all();
    	return view('products.addproductform', compact('categories'));
    }

     function addProduct(Request $request){

    	// the $request calls the validate() method on our request to apply laravel validation rules on a specified request input

    	$request->validate([
    		'product_name' => 'required|string|unique:products', /*:products means the table*/
    		'product_price' => 'required|numeric',
    		'product_description' => 'required|string',
    		'product_category' => 'required',
    		'product_status' => 'required|string',
    		'product_image' => 'required|image'
    	]);

    	// sanitize our request inputs
    	$product_name = htmlspecialchars($request->product_name);
    	$product_price = htmlspecialchars($request->product_price);
    	$product_description = htmlspecialchars($request->product_description);
    	$product_category = htmlspecialchars($request->product_category);
    	$product_status = htmlspecialchars($request->product_status);
    	$product_image = $request->file('product_image');

    	// instantiate product
    	$newProduct = new Product();
    	$newProduct->product_name = $product_name;
    	$newProduct->price = $product_price;
    	$newProduct->description = $product_description;
    	$newProduct->category_id = $product_category;
    	$newProduct->estado = $product_status;

    	
    	$file_name = time().".".$product_image->getClientOriginalExtension();

    	$file_destination = "images/";
  	
    	$product_image->move($file_destination, $file_name);
	
    	$newProduct->image = $file_destination.$file_name;

    	$newProduct->save();
    	return redirect('/productlist');
    }

    function viewProductLists(Request $request){

    	if (isset($request->by_product_name)) {
            $products = Product::orderBy('product_name', $request->by_product_name)->get();
        }
        else {
           $products = Product::all();
        }

        return view('products.productlist', compact('products'));
    }

    function deleteProduct($productId){

    	$deleteProduct = Product::find($productId);
    	$deleteProduct->delete();

    	return redirect('/productlist');
    }

    function viewUpdateForm($productId){
        $product = Product::find($productId);
        $categories = Category::all();
        return view('products.updateproductform', compact('categories', 'product'));
    }

    function updateProduct(Request $request, $productId){
        
        $request->validate([
            'product_name' => 'string', 
            'product_price' => 'numeric',
            'product_description' => 'string',
            'product_status' => 'string'
        ]);

        $product_name = htmlspecialchars($request->product_name);
        $product_price = htmlspecialchars($request->product_price);
        $product_description = htmlspecialchars($request->product_description);
        $product_category = htmlspecialchars($request->product_category);
        $product_status = htmlspecialchars($request->product_status);
       

        $updateProduct = Product::find($productId);
        $updateProduct->product_name = $product_name;
        $updateProduct->price = $product_price;
        $updateProduct->description = $product_description;
        $updateProduct->category_id = $product_category;
        $updateProduct->estado = $product_status;
        
        
        if ($request->file('product_image') != null) {
             $product_image = $request->file('product_image');
             
            
             $file_name = time().".".$product_image->getClientOriginalExtension();
     
             $file_destination = "images/";

    
             $product_image->move($file_destination, $file_name);

             $updateProduct->image = $file_destination.$file_name;
        }

        $updateProduct->save();
        return redirect('/productlist');
    
    }

    function viewCatalogPage(){
        $products = Product::all();
        return view('catalog', compact('products'));
    }


























}
