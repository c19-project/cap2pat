<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category(){
    	return $this->belongsTo('App\Category');
    }


    /*
		Product to Order -> Many to Many
		Multiple products belongs to many orders
		belongsToMany -> only know the pivot table (order_product) and the foreign keys
		withTimestamps() -> 
    */
	public function orders(){
		return $this->belongsToMany('App\Order')->withTimestamps();
	}
}
