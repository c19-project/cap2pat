<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_code');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('purchase_date')->nullable();
            $table->decimal('total_price',8,2);
            $table->unsignedBigInteger('payment_mode_id');
            $table->unsignedBigInteger('status_id');
            $table->timestamps();

            /*Foreign Keys*/

            // User id
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('restrict');

            // payment_mode id
            $table->foreign('payment_mode_id')
            ->references('id')
            ->on('payment_modes')
            ->onUpdate('cascade')
            ->onDelete('restrict');

            // status id
            $table->foreign('status_id')
            ->references('id')
            ->on('statuses')
            ->onUpdate('cascade')
            ->onDelete('restrict');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
