<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	
        	"name" => "admin",
        	"email" => "admin@gmail.com",
        	"password" => Hash::make('adminadmin123'),
        	"role" => 1,
        	"created_at" => Carbon::now(),
        	"updated_at" => Carbon::now()
        ]);
    }
}
