<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function(){
	

	

	// CartController
	Route::resource('cart', 'CartController');
	Route::get('clearcart', 'CartController@clear');

	// OrderController
	Route::resource('orders', 'OrderController');

	// PartnerController
	Route::get('/partner/add', 'PartnerController@viewPartnerAddProductForm');
	Route::post('/partnerproduct/form/add', 'PartnerController@partnerAddProduct');

	// AdminController
	

	Route::get('/userorderhistory', 'AdminController@viewUserTransHistory');

	
});

Route::middleware(['admin'])->group(function(){

    Route::get('/product/form', 'ProductController@viewAddProductPage');

    Route::post('/product/form/add', 'ProductController@addProduct');

	Route::get('/productlist', 'ProductController@viewProductLists');

	Route::get('/product/delete/{productId}', 'ProductController@deleteProduct');

	Route::get('/product/form/update/{productId}', 'ProductController@viewUpdateForm');

	Route::patch('/product/form/update/{productId}', 'ProductController@updateProduct');

	Route::get('/partnerproduct/list', 'AdminController@viewPartnerProducts');

	Route::get('/partnerapprove/{id}', 'AdminController@approveProduct');

	Route::get('/partnerdeleteproduct/{id}', 'AdminController@deletePartnerProducts');

	Route::get('/transactionhistory', 'AdminController@viewOrderHistory');

	Route::get('/businesspartners', 'AdminController@viewBusinessPartners');

	Route::delete('/deleteorderhistory/{id}', 'AdminController@deleteTransactionHistory');

	Route::get('updateorderhistoryform/{id}', 'AdminController@viewUpdateTransactionHistory');

	Route::patch('/updatetransactionhistory/{id}', 'AdminController@updateTransactionHistory');
});

Route::get('/catalog', 'ProductController@viewCatalogPage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
